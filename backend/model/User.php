<?php
include '../../database/Database.php';

class User extends Database{
	public $id_user;
    public $name;
    public $email;
    public $password;
    public $updated_at;
    public $created_at;

    public function save(){
    	$sql = "INSERT INTO user(name, email, password) VALUES(:name, :email, :password);";
    	$db = Database::prepare($sql);
        $db->bindValue('name',  $this->name);
        $db->bindValue('email', $this->email);
        $db->bindValue('password' , $this->password);
    	return $db->execute();
	}

	public function update($obj,$id = null){
		$sql = "UPDATE conteudo SET titulo = :titulo, descricao = :descricao,horario = :horario, curso_id = :curso_id,periodo_id =:periodo_id, disciplina_id = :disciplina_id WHERE id = :id ";
		$consulta = Conexao::prepare($sql);
		$consulta->bindValue('titulo', $obj->titulo);
		$consulta->bindValue('descricao', $obj->descricao);
		$consulta->bindValue('horario' , $obj->horario);
		$consulta->bindValue('curso_id', $obj->curso_id);
		$consulta->bindValue('periodo_id' , $obj->periodo_id);
		$consulta->bindValue('disciplina_id' , $obj->disciplina_id);
		$consulta->bindValue('id', $id);
		return $consulta->execute();
	}

	public function delete($obj,$id = null){
		$sql =  "DELETE FROM conteudo WHERE id = :id";
		$consulta = Conexao::prepare($sql);
		$consulta->bindValue('id',$id);
		$consulta->execute();
	}

	public function find($id = null){

	}

	public function findAll(){
		$sql = "SELECT * FROM user";
		$query = Database::prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

}

?>