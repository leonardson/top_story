<?php
include '../../controller/UserController.php';
$controller = new UserController();

header('Content-Type: application/json');

foreach($controller->findAll() as $item){
	echo json_encode($item);
}