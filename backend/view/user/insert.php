<?php
include '../../controller/UserController.php';
include '../../class/Response.php';
include '../../class/Log.php';

$response = new Response();
$user_controller = new UserController();
$log = new Log();

$name = filter_input(INPUT_POST, 'name');
$email = filter_input(INPUT_POST, 'email');
$password = filter_input(INPUT_POST, 'password');

if ($name && $email && $password) {
    if ($user_controller->insert(array('name' => $name, 'email' => $email, 'password' => $password))) {
        $response->message = 'Operação realizada com sucesso';
        $response->code = 200;
        $response->send();
    } else {
        $response->message = 'Erro encontrado durante o cadastro';
        $response->code = 500;
        $response->send();
    }
} else {
    $response->message = 'Erro na requisição';
    $response->code = 400;
    $response->send();
}
