<?php

class Log {

    private $class_call = '';
    private $file_folder = __DIR__ . '/../logs';
    private $file_name = 'main.log';
    private $filename = '';

    public function __construct() {

        $this->filename = $this->file_folder . '/' . $this->file_name;
        
        if(!file_exists($this->file_folder)) {
            mkdir($this->file_folder, 0777);
        }

        if ( !file_exists($this->filename) ) {
            file_put_contents($this->filename, '');
        }

        $b = debug_backtrace();
        $p = explode('/',$b[0]['file']);
        $file = $p[count($p)-1];
        $this->class_call = $file;

    }

    public function debug($message = '') {
        $final_message = '[DEBUG] [' . date('d/m/Y H:i:s') . '] ' . $this->class_call . ' -> ' . $message;

        $file = fopen($this->filename, 'a');
        fwrite($file, $final_message . "\n");
        fclose($file);
    }

    public function error($message = '') {
        $final_message = '[ERROR] [' . date('d/m/Y H:i:s') . '] ' . $this->class_call . ' -> ' . $message;

        $file = fopen($this->filename, 'a');
        fwrite($file, $final_message . "\n");
        fclose($file);
    }

    public function info($message = '') {
        $final_message = '[INFOR] [' . date('d/m/Y H:i:s') . '] ' . $this->class_call . ' -> ' . $message;

        $file = fopen($this->filename, 'a');
        fwrite($file, $final_message . "\n");
        fclose($file);
    }

}