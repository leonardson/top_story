<?php

class Response {

    public $message;
    public $code;
    public $content;

    public function __construct() {
        $this->message = '';
        $this->code = 500;
    }

    public function send () {
        header('Content-Type: application/json');
        $response_object = array();
        $response_object['message'] = $this->message;
        $response_object['code'] = $this->code;
        $this->content !== NULL ? $response_object['content'] = $this->content : NULL;
        
        echo json_encode($response_object);
        exit(0);
    }
}