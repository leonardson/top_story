<?php
include '../class/Log.php';
include '../../model/User.php';

class UserController {

	private $log;

	public function __construct() {
		$this->log = new Log();
	}

	function insert($obj){
		$user = new User();
		$user->name = $obj['name'];
		$user->email = $obj['email'];
		$user->password = md5($obj['password']);
		
		$this->log->info('Inserir novo usuário');
		
		return $user->save($obj);
	}

	function update($obj,$id){
		$user = new User();
		return $user->update($obj,$id);
	}

	function delete($obj,$id){
		$user = new User();
		return $user->delete($obj,$id);
	}

	function find($id = null){

	}

	function findAll(){
		$user = new User();
		return $user->findAll();
	}
}

?>