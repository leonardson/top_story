import React from 'react';
import './PanelBody.scss';

class PanelBody extends React.Component {
    render() {
        return (
            <div className="PanelBody">
                {this.props.children}
            </div>
        );
    }
}

export default PanelBody;