import React from 'react';
import './PanelTitle.scss';

class PanelTitle extends React.Component {
    render() {
        console.log(this.props);
        return (
            <div className="PanelTitle">
                {this.props.children}
            </div>
        )
    }
}

export default PanelTitle;