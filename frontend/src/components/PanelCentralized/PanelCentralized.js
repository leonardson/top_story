import React from 'react';
import './PanelCentralized.scss';

class PanelCentralized extends React.Component {
    render() {
        return (
            <div className="centralized-block-panel">
                <div className="block-panel">
                    <div className="block-panel-body">
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}

export default PanelCentralized;