import React from 'react';
import './Button.scss';

class Button extends React.Component {
    render() {
        return (
            <div className="Button">
                <button {...this.props}>
                    {this.props.children}
                </button>
            </div>
        );
    }
}

export default Button;