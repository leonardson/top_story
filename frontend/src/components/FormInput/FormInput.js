import React from 'react';
import './FormInput.scss';

class FormInput extends React.Component {
    render() {
        return (
            <div className="FormInput">
                <label htmlFor={this.props.id}>{this.props.label}</label>
                <input {...this.props}/> 
            </div>
        );
    }
}

export default FormInput;