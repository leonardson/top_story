import React from 'react';
import './Login.scss';

import PanelCentralized from '../../components/PanelCentralized/PanelCentralized';
import PanelTitle from '../../components/PanelCentralized/PanelTitle/PanelTitle';
import FormInput from '../../components/FormInput/FormInput';
import Button from '../../components/Button/Button';
import PanelBody from '../../components/PanelCentralized/PanelBody/PanelBody';

class Login extends React.Component {
    render() {
        return (
            <div className="Login">
                <PanelCentralized>
                    <PanelTitle>
                        <h1>Login</h1>
                    </PanelTitle>
                    <PanelBody>
                        <FormInput label="E-mail " type="text" placeholder="Digite seu e-mail" />
                        <FormInput label="Senha " type="password" placeholder="Digite sua senha" />
                    </PanelBody>
                    <div className="footer">
                        <Button>Enviar</Button>
                    </div>
                </PanelCentralized>
            </div>
        );
    }
}

export default Login;