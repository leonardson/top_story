import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';

import Home from './Home/Home';
import Login from './Login/Login';

function App() {
  return (
    <Router>
      <div className="App">
        <Route exact path="/" component={Home} />
        <Route path="/login/" component={Login} />
      </div>
    </Router>
  );
}

export default App;
